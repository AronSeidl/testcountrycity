/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.countriesandcities.services;

import com.progmatic.countriesandcities.daos.CityAutoDao;
import com.progmatic.countriesandcities.daos.CountryAutoDao;
import com.progmatic.countriesandcities.dtos.CityDto;
import com.progmatic.countriesandcities.dtos.CountryDto;
import com.progmatic.countriesandcities.entities.City;
import com.progmatic.countriesandcities.entities.Country;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author peti
 */
@Service
public class CCService {

    private static final Logger LOG = LoggerFactory.getLogger(CCService.class);

    @Autowired
    CountryAutoDao countryAutoDao;

    @Autowired
    CityAutoDao cityAutoDao;

    @Autowired
    private DozerBeanMapper beanMapper;

    @PersistenceContext
    EntityManager em;

    public List<CountryDto> listAllCountries() {
        //TODO get them from the db
        List<Country> allCountries = getTheListOfAllCountries();
        List<CountryDto> allCountryDtos = new ArrayList<>();
        allCountries.forEach((country) -> {
            allCountryDtos.add(beanMapper.map(country, CountryDto.class, "countryMapWithoutCities"));
        });
        return allCountryDtos;
    }

    public List<CityDto> listAllCities() {
        //TODO get them from the db
        List<City> allCities = em.createQuery("select c from City c").getResultList();
        List<CityDto> allCountryDtos = new ArrayList<>();
        allCities.forEach((city) -> {
            allCountryDtos.add(beanMapper.map(city, CityDto.class));
        });
        return allCountryDtos;
    }

    //TODO not finsihed
    public CountryDto detailedCountryData(String id) {
        EntityGraph graph = em.createEntityGraph("graph.countryWithCities");
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("javax.persistence.loadgraph", graph);
        Country c = em.find(Country.class, id, properties);

        if (c == null) {
            return null;
        }
        CountryDto cdto = beanMapper.map(c, CountryDto.class);
        return cdto;
    }

    private List<Country> getTheListOfAllCountries() {
        //TODO load c from the database
        //make somehow sure that all it's cities are also loaded
        List<Country> allCountries = em.createQuery("select c from Country c").getResultList();
        return allCountries;
    }

    //TODO: not implemented
    //delete the country and make sure somehow that all of it's cities are also deleted.
    public void deleteCountry(String id) {
    }

    @Transactional
    public void deleteCity(String id) {
        City c = em.find(City.class, id);
        Country country = c.getCountry();
        City capital = country.getCapital();
        if (capital != null && capital.getId().equals(id)) {
            LOG.info("Capital of {} is deleted.", country.getName());
            country.setCapital(null);
        }
        em.remove(c);
    }

    @Transactional
    public CityDto createCity(CityDto cdto) {
        City city = beanMapper.map(cdto, City.class);
        //TODO create the city in the db.
        //Make sure that it gets an id (maybe use UUID as in BaseEntity) and that the returned cdto also gets it
        return cdto;
    }
}
